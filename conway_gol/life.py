'''
The Cell Players:
    0 : dead cell
    1 : live cell
'''

import numpy as np
from utils.custom_plots import show_img, write_img
from utils.neighbors import getAdjacency

outdir = './output'
imgH, imgW = (72, 128) # a smallish world map
#imgH, imgW = (72*2, 128*2) # a bigger world map

nbStates = 2
adjacency_distance = 1
#initialize = 'GOSPER_GLIDER_GUN'
initialize = 'RANDOMIZE'

# initialize the world

if initialize == 'RANDOMIZE':
    Img = np.random.randint(nbStates, size=(imgH, imgW))
    # reduce number of live cells in first frame
    for ii in range(imgH):
        for jj in range(imgW):
            if Img[ii,jj]==1 and np.random.randint(2):
                Img[ii,jj] = 0
        
elif initialize == 'GOSPER_GLIDER_GUN':
    Img = np.zeros((imgH, imgW), np.uint8)
    ir, ic = (36, 64)
    live_cells = (
        # the main roundish part
        (ir,ic), (ir,ic+2), (ir,ic+3), (ir-1,ic+2), (ir+1,ic+2),
        (ir-2,ic+1), (ir+2,ic+1), (ir-3,ic-1), (ir+3,ic-1),
        (ir-3,ic-2), (ir+3,ic-2), (ir-2,ic-3), (ir+2,ic-3),
        (ir-1,ic-4), (ir+1,ic-4), (ir,ic-4),
        # the left block
        (ir,ic-13), (ir,ic-14), (ir-1,ic-13), (ir-1,ic-14),
        # the near, right rectangle
        (ir-1,ic+6),(ir-1,ic+7),(ir-2,ic+6),(ir-2,ic+7),(ir-3,ic+6),(ir-3,ic+7),
        # the loose pieces
        (ir,ic+8), (ir-4,ic+8),(ir,ic+10),(ir+1,ic+10),(ir-4,ic+10),(ir-5,ic+10),
        # the far, right block
        (ir-2,ic+20),(ir-2,ic+21),(ir-3,ic+20),(ir-3,ic+21)
    )
    ys = [x[0] for x in live_cells]
    xs = [x[1] for x in live_cells]
    Img[ys,xs] = 1


New_Img = np.zeros_like(Img)
iframe = 0
while True:
    if iframe > 0:
        New_Img = Img.copy()
        # identify the types of each cell's neighbors and follow rules to update the world array
        for ii in range(imgH):
            for jj in range(imgW):
                # identify neighbors out to distance = adjacency_distance
                adjacency = getAdjacency(1)
                neighbors = ([(x[0]+ii, x[1]+jj) for x in adjacency])
                # remove any cells that would lie outside the array
                neighbors = [x for x in neighbors if (0<x[0]<imgH) and (0<x[1]<imgW)]
                vals = [Img[x] for x in neighbors]
                counts = sum(vals)
                
                # APPLY THE RULES

                if Img[ii,jj] == 1:
                    # a live cell with fewer than 2 live neighbors dies
                    # a live cell with more than 3 live neighbors dies
                    if counts not in [2,3]:
                        New_Img[ii,jj] = 0
                else:
                    # a dead cell with 3 live neighbors becomes alive
                    if counts == 3:
                        New_Img[ii,jj] = 1


        Img = New_Img
    ImgDisp = np.dstack([Img, Img, Img]) # make an RGB image
    #ImgDisp[Img==1] = np.array([110,110,110]) # color the defender grey
    #ImgDisp[Img==2] = np.array([80,80,255]) # color the mine blue
    #ImgDisp[Img==3] = np.array([100,255,100]) # color the metamorph greenish
    ImgDisp[Img==1] = np.array([255,0,0]) # color the invader red
    show_img(ImgDisp, iframe, ask=False)
    ifstr = str(iframe).zfill(4)
    #write_img(ImgDisp, ifstr, outdir)

    iframe += 1
